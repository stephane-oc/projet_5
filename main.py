#!/usr/bin/env python
# coding: utf-8

from modules import main_menu

menu = main_menu.Menu()

if __name__ == '__main__':
    while True:
        menu.run()
