#!/usr/bin/env python
# coding: utf-8

from modules import common


class DisplayMenu:

    @staticmethod
    def display_menu(items, item_name):
        while True:
            item_id = 1
            for item in items:
                print("[", item_id, "] ", item[0])
                item_id += 1
            try:
                choice = int(input(f"What is your {item_name} ? : "))
                if choice <= len(items):
                    common.Common.clear()
                    print("ok")
                    # print("Your choice : " + str(choice))
                    choice -= 1
                    return choice
            except ValueError:
                pass
            common.Common.clear()
            print("Please choose a number between 1 and " + str(len(items)))
