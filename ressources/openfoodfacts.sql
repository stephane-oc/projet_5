DROP TABLE IF EXISTS public.categories, public.products, public.productcategory, public.backup CASCADE;
DROP TYPE public.nutriScore;

CREATE TYPE public.nutriScore AS ENUM (
    'A',
    'B',
    'C',
    'D',
    'E'
);

CREATE SEQUENCE public.products_productid_seq;

CREATE TABLE public.products (
                productID INTEGER NOT NULL DEFAULT nextval('public.products_productid_seq'),
                description TEXT NOT NULL,
                productCode NUMERIC(13) NOT NULL,
                ProductName VARCHAR(100) NOT NULL,
                nutriScore nutriScore NOT NULL,
                store VARCHAR(100) NOT NULL,
                brand VARCHAR(100) NOT NULL,
                CONSTRAINT products_pk PRIMARY KEY (productID)
);
COMMENT ON COLUMN public.products.description IS 'generic_name';
COMMENT ON COLUMN public.products.productCode IS 'code';


ALTER SEQUENCE public.products_productid_seq OWNED BY public.products.productID;

CREATE UNIQUE INDEX products_idx
 ON public.products
 ( description, productCode, ProductName );

CREATE TABLE public.backup (
                productID INTEGER NOT NULL,
                alternativeProductID INTEGER NOT NULL,
                CONSTRAINT backup_pk PRIMARY KEY (productID, alternativeProductID)
);


CREATE SEQUENCE public.categories_categoryID_seq;

CREATE TABLE public.categories (
                categoryID INTEGER NOT NULL DEFAULT nextval('public.categories_categoryID_seq'),
                categoryName VARCHAR(50) NOT NULL,
                CONSTRAINT categories_pk PRIMARY KEY (categoryID)
);
COMMENT ON COLUMN public.categories.categoryName IS 'categories';


ALTER SEQUENCE public.categories_categoryID_seq OWNED BY public.categories.categoryID;

CREATE UNIQUE INDEX categories_idx
 ON public.categories
 ( categoryName );

CREATE TABLE public.productcategory (
                categoryID INTEGER NOT NULL,
                productID INTEGER NOT NULL,
                CONSTRAINT productcategory_pk PRIMARY KEY (categoryID, productID)
);


ALTER TABLE public.backup ADD CONSTRAINT products_backup_fk
FOREIGN KEY (productID)
REFERENCES public.products (productID)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.backup ADD CONSTRAINT products_backup_fk1
FOREIGN KEY (alternativeProductID)
REFERENCES public.products (productID)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.productcategory ADD CONSTRAINT products_productcategory_fk
FOREIGN KEY (productID)
REFERENCES public.products (productID)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.productcategory ADD CONSTRAINT categories_productcategory_fk
FOREIGN KEY (categoryID)
REFERENCES public.categories (categoryID)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
