#!/usr/bin/env python
# coding: utf-8

import requests
import json


# class used for off interaction
class QueryOpenFF:

    # request json for a product code
    @staticmethod
    def queryinfo(url):
        index = 0
        productlist = []
        productcara = {}
        needinfoproduct = ["product_name", "code", "nutrition_grades", "stores", "brands", "categories", "generic_name_fr"]
        # request product json
        request = requests.get(url)
        # convert result to dictionary
        result = json.loads(request.text)
        productresult = result["products"]
        # Create final list with only used values in some dictionary
        while index < len(productresult):
            for spec in needinfoproduct:
                if spec in productresult[index]:
                    infoproduct = productresult[index][spec]
                    productcara[spec] = infoproduct
                else:
                    break
            productlist.append(productcara)
            productcara = {}
            index += 1
        return productlist
