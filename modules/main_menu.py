#!/usr/bin/env python
# coding: utf-8

import sys
from modules import first_launch
from modules import processing
from modules import query_sql
from modules import category_menu
from modules import common


class Menu:
    # Display a menu and respond to choices when run
    def __init__(self):
        self.choices = {
            "1": self.menu_init_db,
            "2": self.menu_populate_db,
            "3": self.menu_choose_product,
            "4": self.menu_show_backup,
            "0": self.quit
            }
        self.choice = ""
        self.dbfile = "ressources/openfoodfacts.sql"
        self.firstlaunch = first_launch.FirstLaunch()
        self.processing = processing.CheckValues()
        self.querysql = query_sql.Database()
        self.categorymenu = category_menu.DisplayMenu

    @staticmethod
    def display_menu():
        print("""Please choose an action
    1. Initalize database
    2. Populate database
    3. Choose a product
    4. Show saved backup
    0. Quit
        """)

    def run(self):
        # Display the menu and respond to choices
        while True:
            common.Common.clear()
            self.display_menu()
            choice = input("Enter an option: ")
            action = self.choices.get(choice)
            if action:
                action()
            else:
                print("{0} is not a valid choice".format(choice))

    # Called by "Initalize database" menu
    def menu_init_db(self):
        common.Common.clear()
        print("Inialize database ?")
        while True:
            self.choice = input("Do you really want to initalize database ? [y/n] : ").lower()
            if self.choice == "y":
                # First launch (create database skeleton)
                self.firstlaunch.injectsql(self.dbfile)
                break
            elif self.choice == "n":
                self.run()
            else:
                common.Common.clear()
                print("Please choose (y)es or (n)o")

    # Called by "Populate database" menu
    def menu_populate_db(self):
        common.Common.clear()
        print("Populate database ?")
        while True:
            self.choice = input("Do you really want to populate database ? [y/n] : ").lower()
            if self.choice == "y":
                common.Common.clear()
                # Inject database skeleton
                self.processing.populatedatabase()
                input("Injection finished. Press [enter] to continue")
                break
            elif self.choice == "n":
                break
            else:
                common.Common.clear()
                print("Please choose (y)es or (n)o")

    # Called by "Choose a product" menu
    def menu_choose_product(self):
        common.Common.clear()
        menu = self.categorymenu
        print("Please choose a category")
        with self.querysql:
            categories = self.querysql.randomcategory()
        category_choice = menu.display_menu(categories, "category")
        with self.querysql:
            products = self.querysql.randomproduct(categories[category_choice][0])
        product_choice = menu.display_menu(products, "product")
        with self.querysql:
            alternative = self.querysql.findalternative(products[product_choice], categories[category_choice][0])
        print("Your category : " + str(categories[category_choice][0]))
        print("Your product : " + str(products[product_choice][0]))
        if not alternative:
            print("No alternative found")
        else:
            print("Alternative : " + str(alternative))
            while True:
                print(products[product_choice][2])
                print(str(alternative[0][2]))
                self.choice = input("Do you want to save thi result ? [y/n] : ").lower()
                if self.choice == "y":
                    common.Common.clear()
                    with self.querysql:
                        # Inject saved values
                        self.querysql.saveresult(products[product_choice][2], alternative[0][2])
                    input("Result saved. Press [enter] to continue")
                    break
                elif self.choice == "n":
                    break
                else:
                    common.Common.clear()
                    print("Please choose (y)es or (n)o")

    # Called by "Show saved backup" menu
    def menu_show_backup(self):
        common.Common.clear()
        menu = self.categorymenu
        with self.querysql:
            random_saved = self.querysql.randomsaved()
        product_choice = menu.display_menu(random_saved, "product")
        with self.querysql:
            alternative = self.querysql.viewbackup(random_saved[product_choice][1])
        print("Your product : " + str(random_saved[product_choice][0]))
        print("Your saved alternative : " + str(alternative[0][0]))
        input("Press [Enter] to continue...")

    @staticmethod
    def quit():
        common.Common.clear()
        print("Thank you for using this software")
        sys.exit(0)
