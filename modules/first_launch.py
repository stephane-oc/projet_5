#!/usr/bin/env python
# coding: utf-8

# from modules import query_sql
from modules import query_sql
import psycopg2
import sys


class FirstLaunch:
    @staticmethod
    def injectsql(dbfile):
        try:
            dbconn = query_sql.Database()
            with dbconn:
                dbconn.injectfile(dbfile)

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            sys.exit(1)
