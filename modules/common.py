#!/usr/bin/env python
# coding: utf-8

from os import system, name


class Common:
    @staticmethod
    def clear():
        # for windows
        if name == 'nt':
            system('cls')
        # for mac and linux(here, name is 'posix')
        else:
            system('clear')
