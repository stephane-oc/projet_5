#!/usr/bin/env python
# coding: utf-8

import sys
import psycopg2
from ressources import constantes
from modules import query_off
from modules import query_sql


class CheckValues:
    def __init__(self):
        self.caracteristicid = 0
        self.productsRequested = "500"
        self.productId = 0
        self.queryoff = query_off.QueryOpenFF()
        self.dbaction = query_sql.Database()

    def emptyvaluechecker(self, product):
        for i in enumerate(constantes.PRODUCTCARACTERISTIC):
            caracteristic = constantes.PRODUCTCARACTERISTIC[self.caracteristicid]
            if (product.get(caracteristic) is not None) and (product.get(caracteristic) is not ""):
                if self.caracteristicid + 1 == len(constantes.PRODUCTCARACTERISTIC):
                    return True
                else:
                    pass
            else:
                return False
            # print(product.get(caracteristic))
            self.caracteristicid += 1

    def populatedatabase(self):
        # Request OFF droducts in json
        resultquery = self.queryoff.queryinfo('https://fr.openfoodfacts.org/cgi/search.pl?action=process&cc=fr&lc=fr&sort_by=unique_scans_n&page_size=' + self.productsRequested + '&action=display&json=true')
        # Processing json received from OFF (each product individually)
        for j in enumerate(resultquery):
            product = resultquery[self.productId]
            # Check if some values are empty
            resultemptyvalue = self.emptyvaluechecker(product)
            # If there is no empty value, processing sql injection
            if resultemptyvalue is True:
                print(product)
                try:
                    with self.dbaction:
                        # Inject product in database
                        self.dbaction.processdata(product)
                except (Exception, psycopg2.DatabaseError) as error:
                    print(error)
                    sys.exit(1)
            else:
                pass
            self.productId += 1
            print("============================================")
