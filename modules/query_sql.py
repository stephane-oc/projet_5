#!/usr/bin/env python
# coding: utf-8

import psycopg2
import psycopg2.extras
import sys
from modules import config


class Database:

    def __init__(self):
        self.connexion = None
        self.cursor = None
        self.rand = []

    def __enter__(self):
        self.connect()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.connexion is not None:
            self.connexion.close()
            # print('Database connexion closed')
        else:
            pass

    def connect(self):
        try:
            # read connection parameters
            params = config.confread()

            # connect to the PostgreSQL server
            # print('Connecting to the PostgreSQL database')
            self.connexion = psycopg2.connect(**params)
            return self.connexion

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            sys.exit(1)

    def injectfile(self, sqlfile):
        # create a cursor
        self.cursor = self.connexion.cursor()
        # load sql file
        try:
            print('Loading sql file')
            with open(sqlfile, 'r') as file:
                sql_file = file
                print('Execute sql file')
                self.cursor.execute(sql_file.read())
                # commit data
                print('Commit data in database')
                self.connexion.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            sys.exit(1)

    def processdata(self, item):
        # create a cursor
        self.cursor = self.connexion.cursor()
        productname = item['product_name']
        productcode = item['code']
        nutriscore = item['nutrition_grades']
        store = item['stores']
        brand = item['brands']
        categories = item['categories']
        description = item['generic_name_fr']
        # Try to add category and product
        categorylist = categories.split(",")
        for category in categorylist:
            if category.find(':') == -1:
                try:
                    self.cursor.execute("""INSERT INTO public.categories(categoryname)
                                        SELECT %(category)s
                                        WHERE NOT EXISTS (
                                            SELECT 0 FROM public.categories WHERE categoryname=%(category)s
                                        );""", {'category': category})
                    self.cursor.execute("""INSERT INTO public.products(description, productcode, productname, nutriscore, store, brand)
                                        SELECT %(description)s, %(productcode)s, %(productname)s, %(nutriscore)s, %(store)s, %(brand)s
                                        WHERE NOT EXISTS (
                                            SELECT 0 FROM public.products WHERE productcode=%(productcode)s
                                        );""", {'description': description, 'productcode': productcode, 'productname': productname, 'nutriscore': nutriscore.upper(), 'store': store, 'brand': brand})
                    self.cursor.execute("""INSERT INTO productcategory(categoryid, productid)
                                        SELECT
                                        (SELECT categoryid FROM categories WHERE categories.categoryname = %(category)s),
                                        (SELECT productid FROM products WHERE products.productcode = %(productcode)s);""",
                                        {'category': category, 'productcode': productcode})
                except psycopg2.Error as error:
                    print("Error for " + item['product_name'] + " :")
                    print(error)
        self.connexion.commit()

    def randomcategory(self):
        # create a cursor
        self.cursor = self.connexion.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            self.cursor.execute("""SELECT categoryname FROM public.categories ORDER BY RANDOM() LIMIT 5;""")
            self.rand = self.cursor.fetchall()
        except psycopg2.Error as error:
            print(error)
        return self.rand

    def randomproduct(self, category):
        # create a cursor
        self.cursor = self.connexion.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            self.cursor.execute("""SELECT productname,nutriscore,p.productid
                                FROM public.products AS p
                                JOIN public.categories AS c ON c.categoryname = %(category)s
                                JOIN public.productcategory AS pc ON pc.categoryid = c.categoryid
                                WHERE p.productid = pc.productid ORDER BY RANDOM() LIMIT 5;""",
                                {'category': category})
            self.rand = self.cursor.fetchall()
        except psycopg2.Error as error:
            print(error)
        return self.rand

    def findalternative(self, product, category):
        # create a cursor
        self.cursor = self.connexion.cursor(cursor_factory=psycopg2.extras.DictCursor)
        productname = product[0]
        productnutri = product[1]
        print("Name :", productname, " | Nutri :", productnutri, " | Category : ", category)
        try:
            self.cursor.execute("""SELECT productname,nutriscore,p.productid
                                FROM public.products AS p
                                JOIN public.categories AS c ON c.categoryname = %(category)s
                                JOIN public.productcategory AS pc ON pc.categoryid = c.categoryid
                                WHERE p.productid = pc.productid AND nutriscore < %(productnutri)s
                                ORDER BY RANDOM() LIMIT 1;""",
                                {'productnutri': productnutri, 'category': category})
            alternative = self.cursor.fetchall()
            return alternative
        except psycopg2.Error as error:
            print(error)

    def saveresult(self, product, alternative):
        # create a cursor
        self.cursor = self.connexion.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            self.cursor.execute("""INSERT INTO public.backup SELECT %(product)s,%(alternative)s;""",
                                {'product': product, 'alternative': alternative})
            self.connexion.commit()
        except psycopg2.Error as error:
            print(error)

    def randomsaved(self):
        # create a cursor
        self.cursor = self.connexion.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            self.cursor.execute("""SELECT p.productname,p.productid
                                FROM public.products AS p
                                JOIN public.backup AS b ON b.productid = p.productid
                                ORDER BY RANDOM() LIMIT 5;""")
            products = self.cursor.fetchall()
            return products
        except psycopg2.Error as error:
            print(error)

    def viewbackup(self, productid):
        # create a cursor
        self.cursor = self.connexion.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            self.cursor.execute("""SELECT p.productname
                                FROM public.products AS p
                                JOIN public.backup AS b ON b.alternativeproductid = p.productid
                                WHERE b.productid = %(productid)s ORDER BY RANDOM() LIMIT 1;""",
                                {'productid': productid})
            alternative = self.cursor.fetchall()
            return alternative
        except psycopg2.Error as error:
            print(error)
